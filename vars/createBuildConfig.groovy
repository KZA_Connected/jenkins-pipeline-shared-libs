def call (Map vars) {
    // call funtion example: createBuildConfig(
    //                              project: 'name of project where to deploy',
    //                              appName: 'the application name'
    //                              )
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            def models = openshift.process("--filename=openshift/${vars.appName}-buildconfig.yml")
            openshift.apply(models)
        }
    }
}
