def call (Map vars) {
    // call funtion example: buildOpenshiftImage(
    //                              project: 'name of project where to build',
    //                              appName: 'the application name'
    //                              )
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            openshift.selector("bc", vars.appName).startBuild("--from-dir=dist").logs("-f")
        }
    }
}
